/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Robocode_DE;

/**
 *
 * @author lonssi
 */
public class Opponent {
    
    private String name;
    private int index;
    
    public Opponent(String n, int i) {
        name = n;
        index = i;
    }
    
    public String getName() {
        return name;
    }
    
    public int getIndex() {
        return index;
    }
    
}
