
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Robocode_DE;

import java.util.Random;
import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

public class Utility {

    private URL soundToPlay = getClass().getResource("1.wav");
    private AudioClip AC = Applet.newAudioClip(soundToPlay);

    public void Play() {
        AC.play();
    }

    public double getRandomDouble(double u, double l) {
        Random random = new Random();
        return random.nextDouble() * (u - l) + l;
    }

    public int getRandomInt(int u, int l) {
        Random rand = new Random();
        int randomNum = rand.nextInt((u - l) + 1) + l;
        return randomNum;
    }

    public double getGaussian(double m, double d) {
        Random random = new Random();
        return (random.nextGaussian() * d) + m;
    }
    
}


