package Robocode_DE;

import robocode.BattleResults;
import robocode.control.*;
import robocode.control.events.*;

public class BattleRunner {

    RobocodeEngine engine;
    BattleObserver battleObserver;
    BattlefieldSpecification battlefield;

    final static int BATTLE_HANDICAP = 20;

    public BattleRunner(boolean v) {
        engine = new RobocodeEngine(new java.io.File("C:/Robocode"));
        battleObserver = new BattleObserver();
        engine.addBattleListener(battleObserver);
        engine.setVisible(v);
        battlefield = new BattlefieldSpecification(800, 600);
    }

    public void runBattle(EvoBot eBot, int rounds, String opponents) {

        String bot = eBot.getFileName();

        RobotSpecification[] selectedBots = engine.getLocalRepository(bot + opponents);
        BattleSpecification battleSpec = new BattleSpecification(rounds, battlefield, selectedBots);

        // RUN
        engine.runBattle(battleSpec, true);

        BattleResults[] results = battleObserver.getResults();

        double ts = 0, botScore = 0;
        int botFirsts = 0;

        // Gather score
        for (robocode.BattleResults r : results) {
            ts += r.getScore();
            if (bot.equals(r.getTeamLeaderName())) {
                botScore = r.getScore();
                botFirsts = r.getFirsts();
            }
        }

        double totalScore = ts;
        double fraction = (1 / 10) * rounds;

        // Fitness function
        double fitness = 100 * (botFirsts + fraction) / (rounds - botFirsts + fraction) / rounds;
        fitness += 10 * botScore / totalScore;

        eBot.setFitness(fitness);

        System.out.println(eBot.getName() + "\t" + eBot.getFitness());

    }
    
    public void closeEngine() {
        engine.close();
    }
    
}

// based on example from Robocode Control API JavaDocs
class BattleObserver extends BattleAdaptor {

    robocode.BattleResults[] results;

    // Called when the battle is completed successfully with battle results
    public void onBattleCompleted(BattleCompletedEvent e) {
        results = e.getSortedResults();
    }

    // Called when the game sends out an information message during the battle
    public void onBattleMessage(BattleMessageEvent e) {
        //System.out.println("Msg> " + e.getMessage());
    }

    // Called when the game sends out an error message during the battle
    public void onBattleError(BattleErrorEvent e) {
        //System.out.println("Err> " + e.getError());
    }

    public BattleResults[] getResults() {
        return results;
    }

}
