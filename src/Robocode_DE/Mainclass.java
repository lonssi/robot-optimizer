package Robocode_DE;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author lonssi
 */
public class Mainclass {

    public static void main(String[] args) {

        File folder = new File("C:/robocode/robots/sample");
        File[] listOfFiles = folder.listFiles();
        Scanner sc = new Scanner(System.in);
        ArrayList<Opponent> opList = new ArrayList();
        ArrayList<Opponent> finalList = new ArrayList();
        String opponents = "";
        int selection;

        int index = 1;
        for (File listOfFile : listOfFiles) {
            if ((listOfFile.isFile()) && (listOfFile.getName().endsWith(".class"))) {
                String name = listOfFile.getName();
                String[] tokens = name.split("\\.");
                name = tokens[0];
                opList.add(new Opponent(name, index));
                index++;
            }
        }

        System.out.print("Generations: ");
        int gen = sc.nextInt();

        System.out.print("Population size (>50): ");
        int pop = sc.nextInt();

        System.out.print("Rounds per bot (>200): ");
        int rounds = sc.nextInt();

        System.out.print("C (0-1): ");
        double C = sc.nextDouble();

        System.out.print("F (0-1): ");
        double F = sc.nextDouble();
        
        System.out.print("Visible game? (y/n): ");
        String str = sc.next();
        boolean V = false;
        if (str.equals("y")) {
            V = true;
        } else if (str.equals("n")) {
            V = false;
        }

        System.out.println("\nSelect opponent(s):");
        for (Opponent op : opList) {
            System.out.println(op.getIndex() + ". " + op.getName());
        }

        System.out.println("0. Start algorithm");

        while (true) {

            System.out.print("\nSelection: ");
            selection = sc.nextInt();

            if (selection == 0) { 
                if (finalList.isEmpty()) {
                    continue;
                }
                break;
            }

            opponents += ", " + "sample." + opList.get(selection - 1).getName();
            finalList.add(opList.get(selection - 1));

            System.out.println(opList.get(selection - 1).getName() + " added!");

        }

        System.out.println();

        System.out.print("Following robots were selected: ");
        for (Opponent op : finalList) {
            System.out.print(op.getName() + " ");
        }
        System.out.println("\nStarting algorithm...\n");

        DE algorithm = new DE();
        algorithm.setData(gen, pop, rounds, C, F, opponents, V);
        algorithm.DifferentialEvolution();

    }

}
