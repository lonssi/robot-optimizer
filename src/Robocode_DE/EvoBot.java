package Robocode_DE;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EvoBot {

    final static String PATH = new String("C:\\robocode\\robots\\sampleex");
    private String botName, fileName;
    String JARS = new String("C:\\robocode\\libs\\robocode.jar;");
    String PACKAGE = new String("sampleex");
    private String sourceCode = "";
    
    private int ID;
    private int generation;

    private double[] m_energy = new double[3], m_fire = new double[3],
            m_idle = new double[3];
    private double rotation, turn;

    private double fitness, parent1Fitness, parent2Fitness, parent3Fitness;

    public EvoBot(int gen, int botID) {
        generation = gen;
        ID = botID;
        botName = "X_EvoBot_" + generation + "_" + ID;
        fileName = PACKAGE + "." + botName;
    }

    public double[] getEnergy() {
        return m_energy;
    }

    public String getFileName() {
        return fileName;
    }

    public double[] getFire() {
        return m_fire;
    }

    public double getFitness() {
        return fitness;
    }

    public int getGeneration() {
        return generation;
    }

    public int getID() {
        return ID;
    }

    public double[] getIdle() {
        return m_idle;
    }

    public String getName() {
        return botName;
    }

    public double getParent1Fitness() {
        return parent1Fitness;
    }

    public double getParent2Fitness() {
        return parent2Fitness;
    }

    public double getParent3Fitness() {
        return parent3Fitness;
    }

    public double getRotation() {
        return rotation;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public double getTurn() {
        return turn;
    }

    public void setEnergy(double[] m_e) {
        m_energy = m_e;
    }

    public void setFileName(String fn) {
        fileName = fn;
    }

    public void setFire(double[] m_f) {
        m_fire = m_f;
    }

    public void setFitness(double f) {
        fitness = f;
    }

    public void setID(int i) {
        ID = i;
    }

    public void setIdle(double[] m_i) {
        m_idle = m_i;
    }

    public void setName(String n) {
        botName = n;
    }

    public void setParent1Fitness(double f) {
        parent1Fitness = f;
    }

    public void setParent2Fitness(double f) {
        parent2Fitness = f;
    }

    public void setParent3Fitness(double f) {
        parent3Fitness = f;
    }

    public void setRotation(double r) {
        rotation = r;
    }

    public void setTurn(double t) {
        turn = t;
    }

    public void setVariables(double[] m_i, double[] m_e, double[] m_f, double r, double t) {

        m_idle = m_i;
        m_energy = m_e;
        m_fire = m_f;
        rotation = r;
        turn = t;

    }

    public void updateData(int gen) {
        generation = gen;
        botName = "X_EvoBot_" + generation + "_" + ID;
        fileName = PACKAGE + "." + botName;
    }

    public String setSourceCode() {

        sourceCode
                = "package " + PACKAGE + ";\n"
                + "\n"
                + "\n"
                + "import robocode.HitByBulletEvent;\n"
                + "import robocode.HitWallEvent;\n"
                + "import robocode.RateControlRobot;\n"
                + "import robocode.ScannedRobotEvent;\n"
                + "\n"
                + "\n"
                + "/**\n"
                + " * This is a sample of a robot using the RateControlRobot class\n"
                + " * \n"
                + " * @author Joshua Galecki (original) modified by Lauri Rapo\n"
                + " */\n"
                + "public class " + botName + " extends RateControlRobot {\n"
                + "\n"
                + "	double[] m_idle = {" + m_idle[0] + ", " + m_idle[1] + ", " + m_idle[2] + "};\n"
                + "	double[] m_energy = {" + m_energy[0] + ", " + m_energy[1] + ", " + m_energy[2] + "};\n"
                + "	double[] m_fire = {" + m_fire[0] + ", " + m_fire[1] + ", " + m_fire[2] + "};\n"
                + "	double rotation = " + rotation + ";\n"
                + "	double turn = " + turn + ";\n"
                + "\n"
                + "	int turnCounter;\n"
                + "	public void run() {\n"
                + "\n"
                + "		turnCounter = 0;\n"
                + "		setGunRotationRate(rotation);\n"
                + "		\n"
                + "		while (true) {\n"
                + "			if (turnCounter % 64 == 0) {\n"
                + "				// Straighten out, if we were hit by a bullet and are turning\n"
                + "				setTurnRate(m_idle[0]);\n"
                + "				// Go forward with a velocity of 4\n"
                + "				setVelocityRate(m_idle[1]);\n"
                + "			}\n"
                + "			if (turnCounter % 64 == 32) {\n"
                + "				// Go backwards, faster\n"
                + "				setVelocityRate(m_idle[2]);\n"
                + "			}\n"
                + "			turnCounter++;\n"
                + "			execute();\n"
                + "		}\n"
                + "	}\n"
                + "\n"
                + "	public void onScannedRobot(ScannedRobotEvent e) {\n"
                + "		\n"
                + "		double en = getEnergy();\n"
                + "\n"
                + "		if (en > m_energy[0]) {\n"
                + "			fire(m_fire[0]);\n"
                + "		} else if (en > m_energy[1]) {\n"
                + "			fire(m_fire[1]);\n"
                + "		} else if (en > m_energy[2]) {\n"
                + "			fire(m_fire[2]);\n"
                + "		}\n"
                + "\n"
                + "	}\n"
                + "\n"
                + "	public void onHitByBullet(HitByBulletEvent e) {\n"
                + "		// Turn to confuse the other robot\n"
                + "		setTurnRate(turn);\n"
                + "	}\n"
                + "	\n"
                + "	public void onHitWall(HitWallEvent e) {\n"
                + "		// Move away from the wall\n"
                + "		setVelocityRate(-1 * getVelocityRate());\n"
                + "	}\n"
                + "}";

        return sourceCode;

    }

    
    // The following methods are the work of Ted Hunter
    // https://github.com/toxicblu/robocode-gp
    public String compile() {

        try {
            FileWriter fstream = new FileWriter(PATH + "\\" + botName + ".java");
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(sourceCode);
            out.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }

        while (true) {
            // Compile code
            try {
                execute("javac -cp " + JARS + " " + PATH + "\\" + botName + ".java");
                break;
            } catch (Exception e) {
                try {
                    System.out.println("Compiling failed! Trying again...");
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(EvoBot.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return (PATH + "\\" + botName + ".class");
    }

    public static void clearBots(int gen, int pop_size, int bestID, EvoBot best, EvoBot[] pop, int imp) {
        File oldJava, oldClass;
        for (int i = 0; i < pop_size; i++) {

            if (i == bestID && imp == 0) {
                continue;
            }

            oldJava = new File(PATH + "\\" + "X_EvoBot_" + gen + "_" + i + ".java");
            oldClass = new File(PATH + "\\" + "X_EvoBot_" + gen + "_" + i + ".class");

            oldJava.delete();
            oldClass.delete();
        }
    }

    private static void execute(String command) throws Exception {
        Process process = Runtime.getRuntime().exec(command);
        printMsg(command + " stdout:", process.getInputStream());
        printMsg(command + " stderr:", process.getErrorStream());
        process.waitFor();
        if (process.exitValue() != 0) {
            System.out.println(command + "exited with value " + process.exitValue());
        }
    }

    private static void printMsg(String name, InputStream ins) throws Exception {
        String line = null;
        BufferedReader in = new BufferedReader(new InputStreamReader(ins));
        while ((line = in.readLine()) != null) {
            System.out.println(name + " " + line);
        }
    }

}
