1. Install robocode to C:/robocode
	download: http://robocode.sourceforge.net/

2. Open an IDE that can run Java - I used NetBeans: https://netbeans.org/
	also - Java runtime environment needs to be installed: https://java.com/en/download/index.jsp


*the following instructions are for NetBeans*


3. In NetBeans, go to Tools -> Libraries

4. Press 'New Library' and type robocode to the name field

5. Press Add Jar/Folder

6. Go to C:/robocode/libs and select robocode.jar

7. Close the Libraries window and go to File ->  Open Project

8. Choose the folder Robocode_DE


NOTE: 	The robots created by the algorithm are in folder C:/robocode/robots/sampleex/
	also, results.txt file is created each time which contains the average and best fitness
	of the population per generation.

	If you want the evaluations to be visible, open BattleRunner.java and go to line 19
	edit 'engine.setVisible(false);' to 'engine.setVisible(true);'
	sometimes this may cause some errors


