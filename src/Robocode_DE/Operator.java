/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Robocode_DE;

/**
 *
 * @author lonssi
 */
public class Operator {

    private Utility rnd;

    public Operator() {
        rnd = new Utility();
    }

    public EvoBot createChild(EvoBot[] pop, int g, int id, double C, double F) {

        EvoBot child = new EvoBot(g, id);
        int s = pop.length - 1;

        // Generate 3 random different indexes
        int i1, i2, i3;
        i1 = rnd.getRandomInt(s, 0);
        i2 = i1;
        while (i2 == i1) {
            i2 = rnd.getRandomInt(s, 0);
        }
        i3 = i2;
        while ((i3 == i2) || (i3 == i1)) {
            i3 = rnd.getRandomInt(s, 0);
        }

        EvoBot p1 = pop[i1];
        EvoBot p2 = pop[i2];
        EvoBot p3 = pop[i3];

        child.setParent1Fitness(p1.getFitness());
        child.setParent2Fitness(p2.getFitness());
        child.setParent3Fitness(p3.getFitness());

        double[] m_idle = new double[3], m_energy = new double[3],
                m_fire = new double[3];
        double rotation, turn;

        if (rnd.getRandomDouble(1, 0) < C) {

            for (int i = 0; i < m_idle.length; i++) {
                m_idle[i] = p3.getIdle()[i] + F * (p1.getIdle()[i] - p2.getIdle()[i]);
            }

            for (int i = 0; i < m_energy.length; i++) {
                m_energy[i] = p3.getEnergy()[i] + F * (p1.getEnergy()[i] - p2.getEnergy()[i]);
                if (m_energy[i] < 0) {
                    m_energy[i] = -m_energy[i];
                }
            }

            for (int i = 0; i < m_fire.length; i++) {
                m_fire[i] = p3.getFire()[i] + F * (p1.getFire()[i] - p2.getFire()[i]);
                if (m_fire[i] < 0) {
                    m_fire[i] = -m_fire[i];
                }
            }

            rotation = p3.getRotation() + F * (p1.getRotation() - p2.getRotation());

            turn = p3.getTurn() + F * (p1.getTurn() - p2.getTurn());

        } else {

            m_idle = p3.getIdle();
            m_energy = p3.getEnergy();
            m_fire = p3.getFire();
            rotation = p3.getRotation();
            turn = p3.getTurn();

        }

        child.setVariables(m_idle, m_energy, m_fire, rotation, turn);

        return child;
    }

}
