/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Robocode_DE;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author 0403231
 */
public class DE {

    private int G_max, pop_size, rounds;
    private double C, F, avgFitness[];
    private String opponents;
    private EvoBot pop[], child[], tournament[], best;
    private boolean visible;

    public void setData(int g, int p, int r, double c, double f, String o, boolean v) {
        G_max = g + 1;
        pop_size = p;
        rounds = r;
        C = c;
        F = f;
        opponents = o;
        avgFitness = new double[G_max];
        pop = new EvoBot[pop_size];
        child = new EvoBot[pop_size];
        tournament = new EvoBot[pop_size * 2];
        visible = v;
    }

    public void DifferentialEvolution() {

        // Average fitness of generation
        double fitnessPerGen;

        // Counter for improvement
        int improvement = 0;

        // Create differential evolution operator
        Operator opr = new Operator();

        // Create results.txt
        createSave();

        // Initialize best individual
        best = new EvoBot(0, 0);
        best.setFitness(0);

        // Initialization and evaluation of population
        initialization();

        // The main evolution loop
        for (int g = 1; g < G_max; g++) {

            System.out.println("\n**********************\nGeneration: " + g
                    + "\n**********************\n");

            // Generate new child population
            for (int i = 0; i < pop_size; i++) {
                // Add bot to child population
                child[i] = opr.createChild(pop, g, i, C, F);
            }

            // Compile and evaluate child population
            System.out.println("\nCompiling...");
            compileChildPop();
            System.out.println("Evaluating:");
            evaluateChildPop();

            // Print out the original population and the new child population
            System.out.println("\nParent pop\t\t\t Child pop");
            for (int i = 0; i < pop_size; i++) {
                System.out.printf("%-24s ", pop[i].getFitness());
                System.out.printf("\t %s\n", child[i].getFitness());
            }

            // Count successful mutations
            int count = 0;
            for (int i = 0; i < pop_size; i++) {
                if ((child[i].getFitness() > child[i].getParent1Fitness())
                        && (child[i].getFitness() > child[i].getParent2Fitness())
                        && (child[i].getFitness() > child[i].getParent3Fitness())) {
                    count++;
                }
            }

            // Selection method
            elitistSelection();
            //tournamentSelection();

            // Print out the surviving population
            System.out.println("\n\nSurviving population");
            for (int i = 0; i < pop_size; i++) {
                System.out.printf("%s\t\t%s\n", pop[i].getName(), pop[i].getFitness());
            }

            // Average fitness between all individuals in current generation
            fitnessPerGen = 0;
            for (int i = 0; i < pop_size; i++) {
                fitnessPerGen += pop[i].getFitness();
            }
            avgFitness[g] = fitnessPerGen / pop_size;

            // Save the new best individual
            if (pop[0].getFitness() > best.getFitness()) {
                best.setFitness(pop[0].getFitness());
                best.setName(pop[0].getName());
                best.setID(pop[0].getID());
                best.updateData(pop[0].getGeneration());
                best.setVariables(pop[0].getIdle(),
                        pop[0].getEnergy(), pop[0].getFire(),
                        pop[0].getRotation(),
                        pop[0].getTurn());
                best.setSourceCode();
                improvement = 0;
            } else {
                improvement += 1;
            }

            System.out.println("\nSuccessful mutations: " + count + "/" + pop_size);
            System.out.println("Average fitness of generation " + g + ": " + avgFitness[g]);
            System.out.println("Best individual so far: " + best.getName() + "\nFitness: " + best.getFitness());
            System.out.println();

            if (improvement > 0) {
                System.out.println("No improvement in " + improvement + " generations.\n");
            }

            // Update results.txt
            saveBestFitnessPerGen(pop[0].getFitness(), avgFitness[g], g);

            // Delete redundant bot files
            EvoBot.clearBots(g, pop_size, pop[0].getID(), best, pop, improvement);

        }

        // Play sound when done
        Utility util = new Utility();
        util.Play();

    }

    private void initialization() {

        Utility random = new Utility();

        for (int i = 0; i < pop_size; i++) {

            double[] m_idle = new double[3], m_energy = new double[3],
                    m_fire = new double[3];
            double rotation, turn;

            for (int j = 0; j < m_idle.length; j++) {
                m_idle[j] = random.getGaussian(0, 10);
            }

            for (int j = 0; j < m_energy.length; j++) {
                m_energy[j] = random.getRandomDouble(100, 0);
            }

            for (int j = 0; j < m_fire.length; j++) {
                m_fire[j] = random.getRandomDouble(10, 0.5);
            }

            rotation = random.getGaussian(0, 15);
            turn = random.getGaussian(0, 15);

            pop[i] = new EvoBot(0, i);
            pop[i].setVariables(m_idle, m_energy, m_fire, rotation, turn);
            pop[i].setSourceCode();
            pop[i].compile();

        }

        evaluateInitialPop();

    }

    private void evaluateInitialPop() {

        System.out.println("Evaluating initial population:");
        BattleRunner sim = new BattleRunner(visible);
        for (EvoBot eb : pop) {
            sim.runBattle(eb, rounds, opponents);
        }
        sim.closeEngine();

    }

    private void compileChildPop() {

        for (EvoBot eb : child) {
            eb.setSourceCode();
            eb.compile();
        }

    }

    private void evaluateChildPop() {

        BattleRunner sim = new BattleRunner(visible);
        for (EvoBot eb : child) {
            sim.runBattle(eb, rounds, opponents);
        }
        sim.closeEngine();

    }

    private void createSave() {

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(EvoBot.PATH + "\\results.txt"));
            out.write("Generations: " + (G_max - 1) + "\r\nRounds: " + rounds + "\r\nPopulation: "
                    + pop_size + "\r\nC: " + C + "\r\nF: " + F + "\r\n\r\n");
            out.write("Generation, Best fitness, Avg fitness\r\n");
            out.close();
        } catch (FileNotFoundException e) {
            System.exit(0);
        } catch (IOException e) {
            System.exit(0);
        }

    }

    private void saveBestFitnessPerGen(double fit, double avg, int g) {

        FileWriter dataStream;

        try {
            dataStream = new FileWriter(EvoBot.PATH + "\\results.txt", true);
            dataStream.append(g + ", " + fit + ", " + avg + "\r\n");
            dataStream.close();
        } catch (IOException e) {
            System.exit(0);
        }

    }

    private void elitistSelection() {

        EvoBot temp;

        System.arraycopy(pop, 0, tournament, 0, pop_size);

        for (int i = pop_size; i < 2 * pop_size; i++) {
            tournament[i] = child[i - pop_size];
        }

        for (int i = 0; i < 2 * pop_size; i++) {
            for (int j = 2 * pop_size - 1; j > i; j--) {
                if (tournament[j - 1].getFitness() < tournament[j].getFitness()) {
                    temp = tournament[j - 1];
                    tournament[j - 1] = tournament[j];
                    tournament[j] = temp;
                }
            }
        }

        System.arraycopy(tournament, 0, pop, 0, pop_size);

    }

}
